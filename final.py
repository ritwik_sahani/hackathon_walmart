import csv
import sys
import numpy
import random
from sklearn.preprocessing import Imputer,minmax_scale
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier,VotingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis



train_file=open('training_set.csv','r')
test_file=open('test_set.csv','r')

rw=train_file.readline()
rw1=test_file.readline()
# print rw
reader=csv.reader(train_file)
reader1=csv.reader(test_file)
train=[ {'id':int(row[0]),'segment':row[1],'attr': [numpy.nan if fl=='NA' else float(fl) for fl in row[2:]]} for row in reader ]	
test=[ {'id':int(row[0]),'attr': [numpy.nan if fl=='NA' else float(fl) for fl in row[1:]]} for row in reader1]	

test_file.close()
train_file.close()

removable=[]
l=len(train[0]['attr'])
print l
for i in range(l):
	f=True
	for j in train:
		if numpy.isnan(j['attr'][i])==False:
			f=False
			break
	if f==True:
		removable.append(i)
print removable


imp=Imputer(strategy='median',axis=0)

X=[row['attr'] for row in train]
X=imp.fit_transform(X)


for i in range(len(train)):
	train[i]['attr']=X[i].tolist()

imp1=Imputer(strategy='median',axis=0)
X1=[row['attr'] for row in test]
X1=imp1.fit_transform(X1)
for i in range(len(test)):
	test[i]['attr']=X1[i].tolist()


dataset=[i['attr'] for i in train]
target=[i['segment'] for i in train]
dataset_test=[i['attr'] for i in test]


rf=RandomForestClassifier(n_estimators=15)
rf.fit(dataset,target)

ld=LinearDiscriminantAnalysis()
ld.fit(dataset,target)

lr=LogisticRegression()
lr.fit(dataset,target)

svm=SVC()
svm.fit(dataset,target)

vc=VotingClassifier([('rf',rf),('ld',ld),('lr',lr),('svm',svm)])
vc.fit(dataset,target)
predicted=vc.predict(dataset_test).tolist()


final=[]
for i in range(len(test)):
	final.append((test[i]['id'],predicted[i]))

write_file=open('sample_submission.csv','w')
wr=csv.writer(write_file,delimiter=',',quotechar="'")
wr.writerow(('Store_ID','segment_name'))
for i in final:
	wr.writerow(i) 
write_file.close()